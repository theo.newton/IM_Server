import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * Created by Theo Newton 27th December 2016
 */
public class Server extends JFrame{

	private static final long serialVersionUID = 1L;
	private int portNum = 8008;
	private int queueLength = 100;
    private JTextField userText;
    private JTextArea chatWindow;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private ServerSocket server;
    private Socket connection;

    /**
     * Constructor method for the chat's serverside.
     */
    public Server(){
    	super("Scamander Chat Server");
       	userText = new JTextField();
    	// prevent user input when not connected
    	userText.setEditable(false);
    	userText.addActionListener(
    		new ActionListener(){
    			public void actionPerformed(ActionEvent event){
    				sendMessage(event.getActionCommand());
    				// Message sent so reset field
    				userText.setText("");
    			}
    		}
    			);
    	add(userText, BorderLayout.NORTH);
    	chatWindow = new JTextArea();
    	add(new JScrollPane(chatWindow));
    	setSize(500,250);
    	setVisible(true);
    }


    public void run(){
    	try{
    		server = new ServerSocket(portNum, queueLength);
    		while(true){
    			try{
    				waitForConnection();
    				initStream();
    				chatHandler();
    			}
    			catch(EOFException e){
    				showMessage("\n Server terminated the connection.");
    			}
    			finally{
    				closeAndTidy();
    			}
    		}
    	}
    	catch(IOException e){
    		e.printStackTrace();

    	}
    }

    /*
     * 	Wait for a user to connect to the server
     */
    private void waitForConnection() throws IOException{
    	showMessage("Waiting for a user to connect... \n");
    	connection = server.accept();
    	showMessage("Connected to " + connection.getInetAddress().getHostName());
    }

    /*
     * 	After we have a connection, set up the I/O streams
     */
    private void initStream() throws IOException{
    	output = new ObjectOutputStream(connection.getOutputStream());
    	output.flush();
    	input = new ObjectInputStream(connection.getInputStream());
    	showMessage("\n Stream initialization complete... \n");
    }

    /*
     * Allows communication while checking the client has not send a termination message
     */
    private void chatHandler() throws IOException{
    	String msg = "You are now connected";
    	sendMessage(msg);
    	allowMsgText(true);
    	do{
    		try{
    			// input is the client's input
    			msg = (String) input.readObject();
    			showMessage("\n" + msg);
    		}
    		catch(ClassNotFoundException e){
    			showMessage("\n ERROR: Invalid message data");
    		}
    	}while(!msg.equals("CLIENT - END"));
    }

    /*
     * 	Close streams and sockets
     */
    private void closeAndTidy(){
    	showMessage("\n Terminating connections \n");
    	allowMsgText(false);
    	try{
    		output.close();
    		input.close();
    		connection.close();
    	}
    	catch(IOException e){
    		e.printStackTrace();
    	}
    }

    /*
     * 	Send a message to the client
     * 	@param	msg	The string that the server wishes to send to the client
     */
    private void sendMessage(String msg){
    	try{
    		output.writeObject("SERVER - " + msg);
    		output.flush();
    		showMessage("\n SERVER - " + msg);
    	}
    	catch(IOException e){
    		chatWindow.append("\n ERROR: Message failed to send \n");
    	}

    }

    /*
     * 	The text that will be displayed in the chat window
     * 	@param	msgText	The string of text to display on the chat client
     */
    private void showMessage(final String msgText){
    	SwingUtilities.invokeLater(
    			// Create our thread
    			new Runnable(){
    				public void run(){
    					chatWindow.append(msgText);
    				}
    			}
    	);
    }

    /*
     *  Allow the user to type text
     *  @param isAllowed	Change whether the user has permission to type or not
     */
    private void allowMsgText(final boolean isAllowed){
    	SwingUtilities.invokeLater(
    			// Create our thread
    			new Runnable(){
    				public void run(){
    					userText.setEditable(isAllowed);
    				}
    			}
    	);
    }
}
