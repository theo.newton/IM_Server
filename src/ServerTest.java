import javax.swing.JFrame;

/*
 * Created by Theo Newton 27th December 2016
 */

public class ServerTest {

	public static void main(String[] args) {
		Server cs = new Server();
		cs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cs.run();

	}

}
